package org.bitbucket.backspace119.generallib.data;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;

/**
 * Created by Bryan on 3/9/2017.
 */
public class JSONFileHandler {

    private File file;
    public JSONFileHandler(String file)
    {
        this.file = new File(file);
        if(!this.file.exists())
        {
            try {
                this.file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    public JSONObject getJSON() throws IOException, ParseException {
        BufferedInputStream is = new BufferedInputStream(new FileInputStream(file));
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        String line = reader.readLine();
        reader.close();
        return (JSONObject) new JSONParser().parse(line);
    }


    public void writeJSON(JSONObject jo) throws FileNotFoundException {
        BufferedOutputStream os = new BufferedOutputStream(new FileOutputStream(file));
        PrintWriter writer = new PrintWriter(os);
        writer.println(jo.toJSONString());
        writer.close();
    }
}
