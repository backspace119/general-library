package org.bitbucket.backspace119.generallib.data;

import com.mchange.v2.c3p0.ComboPooledDataSource;

import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;



/**
 * the following is a wrapper for some common MySQL stuff it is not intended to
 * do anything very advanced in handling the database
 *
 * Extend this class with your own class and implement the mapResults method.
 *
 * @author backspace119
 *
 */
public abstract class SQLManager {

    final String url;
    final String username;
    final String password;

    /**
     *
     * @param host
     *            the hostname of your database
     * @param database
     *            the database name to query from
     * @param username
     *            the username for the database
     * @param password
     *            the password for the username
     * @param port
     *            the port which the database runs on
     */
    private ComboPooledDataSource cpds;
    public SQLManager(String host, String database, String username,
                      String password, int port) {
        System.setProperty("com.mchange.v2.log.MLog", "com.mchange.v2.log.FallbackMLog");
        System.setProperty("com.mchange.v2.log.FallbackMLog.DEFAULT_CUTOFF_LEVEL", "WARNING");

        cpds = new ComboPooledDataSource();

        this.url = "jdbc:mysql://" + host + ":" + port + "/" + database;
        this.password = password;
        this.username = username;

        try {
            cpds.setDriverClass( "com.mysql.cj.jdbc.Driver");
        } catch (PropertyVetoException e1) {
            //e1.printStackTrace();
        } //loads the jdbc driver
        cpds.setJdbcUrl(url);
        cpds.setUser(username);
        cpds.setPassword(password);

        // the settings below are optional -- c3p0 can work with defaults
        cpds.setMinPoolSize(1);
        cpds.setAcquireIncrement(1);
        cpds.setMaxPoolSize(5);
        cpds.setMaxIdleTime(0);
        cpds.setInitialPoolSize(1);
        cpds.setAcquireRetryAttempts(10);
        cpds.setAcquireRetryDelay(15);

        // The DataSource cpds is now a fully configured and usable pooled DataSource



    }

    Connection conn;
    ResultSet rs;
    PreparedStatement ps;

    /**
     * performs simple query RESULT SET CAN RETURN NULL! CHECK THIS!
     *
     * @param query
     *            the sql statement to pass to the server as a query
     * @return the ResultSet that this querry received
     */
    public ResultSet querry(String query) {
        try {
            if (conn.isClosed()) {
                conn = getConnection();
            }
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rs;
    }

    /**
     * maps the ResultSet to your custom object that you've implemented in the
     * mapResults method the object you make should just be a container for data
     * you want to put in and pull out of the database
     *
     * @param query
     *            the sql statement to pass to the server as a query
     * @return the container object that you have made to hold data from a query
     */
    public Object querryAndMap(String query,Object... data) {
        try {
            if (conn.isClosed()) {
                conn = getConnection();
            }
            ps = conn.prepareStatement(query);
            setValues(ps,data);
            rs = ps.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return mapResults(rs);
    }

    /**
     * This is a utility statement that does not necessarily ever have to be used by the end user.
     * It is used internally in the library and can be ignored.
     * @param connection
     * @param sql
     * @param returnGeneratedKeys
     * @param values
     * @return
     * @throws SQLException
     */
    public static PreparedStatement prepareStatement(Connection connection,
                                                     String sql, boolean returnGeneratedKeys, Object... values)
            throws SQLException {

        PreparedStatement preparedStatement = connection.prepareStatement(sql,
                returnGeneratedKeys ? 1 : 2);
        setValues(preparedStatement, values);
        return preparedStatement;
    }
    /**
     * Much like prepareStatement() this is internal and can be ignored.
     * @param preparedStatement
     * @param values
     * @throws SQLException
     */
    public static void setValues(PreparedStatement preparedStatement,
                                 Object... values) throws SQLException {
        for (int i = 0; i < values.length; i++) {
            preparedStatement.setObject(i + 1, values[i]);
        }
    }
    /**
     * This allows you to manipulate data on the database (INSERT, UPDATE, SET, etc)
     * @param sql the SQL statement to be executed
     * @param data the data that pertains to said statement, must be data that matches your table types.
     */
    public void manipulate(String sql, Object... data) {
        try {
            this.conn = getConnection();
            this.ps = prepareStatement(this.conn, sql, false, data);
            this.ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally{
            closeAll();
        }
    }

    /**
     * make sure to close your resources when you're done with them this closes
     * the connection too, so do not use this unless you are done with the
     * connection
     */
    public void closeAll() {
        try {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * this shuts down the pool object making it unusable, do not call this unless you are completely done with the database object
     */
    public void shutdown()
    {
        cpds.close();
    }

    /**
     * this closes the "data" from an SQL connection, the ResultSet and the
     * PreparedStatement. Use this if you do not want to close the connection
     * yet.
     */
    public void closeData() {
        try {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public Connection getConnection() throws SQLException {
        if(conn != null)
        {

            if(!conn.isClosed())
            {
                return conn;
            }
        }

        return cpds.getConnection();
    }

    /**
     *
     * @return the url constructed from the parameters given to the constructor
     */
    public String getURL()
    {
        return url;
    }
    /**
     * This should take any result set that has been returned and map it into a usable Java object for the method querryAndMap()
     * @param resultset the ResultSet that was returned by the querry
     * @return the custom object that has encapsulated the data from the ResultSet
     */
    public abstract Object mapResults(ResultSet resultset);
}
