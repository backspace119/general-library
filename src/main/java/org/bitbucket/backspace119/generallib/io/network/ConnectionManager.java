package org.bitbucket.backspace119.generallib.io.network;

import java.util.List;

/**
 * Created by Bryan on 5/31/2017.
 */
public interface ConnectionManager {

    boolean sendPacket(Packet p);
    boolean sendPacket(Object o);
    boolean isConnected();
    List<Packet> receivedPackets();
    void listen(String line);
    String getID();

}
