package org.bitbucket.backspace119.generallib.io.network;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bryan on 5/8/2017.
 */
public class FIFOQueue implements NetworkQueue {

    private List<Packet> queue = new ArrayList<>();
    private Packet previous;
    @Override
    public Packet getNext() {
        if(queue.isEmpty()) return null;
        previous = queue.get(0);
        queue.remove(0);
        return previous;
    }

    @Override
    public Packet getPrevious() {
        return previous;
    }

    @Override
    public List<Packet> getAll() {
        return queue;
    }

    @Override
    public void clear() {
        queue.clear();
    }

    @Override
    public void add(Packet p) {
        queue.add(p);
    }
}
