package org.bitbucket.backspace119.generallib.io.network;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Map;

/**
 * Created by Bryan on 4/25/2017.
 */
public interface NetworkManager {


    void tick();
    ServerSocket startServer(int port);
    Socket connect(String ip, int port) throws IOException;
    boolean broadcastPacket(Packet p);
    boolean connectionStatus(String ID);
    void stopNet();
    void start();
    ConnectionManager getConnection(String ID);
    boolean broadcastAllBut(Packet p, String ID);
    boolean broadcastPacket(Object o);
    boolean broadcastAllBut(Object o, String ID);


}
