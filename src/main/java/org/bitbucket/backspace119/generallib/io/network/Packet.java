package org.bitbucket.backspace119.generallib.io.network;

import java.util.Map;

/**
 * Created by Bryan on 4/26/2017.
 *
 * JSON based Packet for sending JSON payloads
 */
public interface Packet {

    Map<String,String> getData();
    void setData(Map<String,String> data);

    void process(ConnectionManager connMan);
    String toJSON();



}
