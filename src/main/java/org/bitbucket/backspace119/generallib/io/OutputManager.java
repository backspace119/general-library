package org.bitbucket.backspace119.generallib.io;

import org.bitbucket.backspace119.generallib.flow.ManagedThread;
import org.bitbucket.backspace119.generallib.flow.ThreadManager;

import java.util.*;

/**
 * Created by Bryan on 3/22/2017.
 */
public abstract class OutputManager extends ManagedThread{

    private long tickDuration = 0;
    public OutputManager(ThreadManager tMan, String ID, long tickDuration)
    {
        super(tMan,ID);
        this.tickDuration = tickDuration;
    }

    private List<String> stringOutput = Collections.synchronizedList(new ArrayList<>());
    protected Map<String,String> mappedOutput = new HashMap<>();

    private Object sOlock = new Object();
    private Object mOlock = new Object();

    public abstract void tick();

    @Override
    protected void preLoop()
    {

    }

    public void output(String output)
    {
        stringOutput.add(output);
    }

    /**
     *
     * @return a copy of the List containing all output added to this manager. This allows easy thread safety, although keep in mind no changes made to this will affect the actual List
     */
    protected  List<String> getStringOutput()
    {
        List<String> copy = new ArrayList<>();
        synchronized (stringOutput) {

            for (String s : stringOutput) {
                copy.add(s);
            }
        }
        return copy;
    }


    public synchronized void mappedOutput(String key,String output)
    {
        mappedOutput.put(key,output);
    }

    protected  synchronized  Map<String,String> getMappedOutput()
    {
        Map<String,String> copy = new HashMap<>();
        for(String key:mappedOutput.keySet())
        {
            copy.put(key,mappedOutput.get(key));
        }
        return copy;
    }

    @Override
    protected void postLoop()
    {

    }
    private long lastTick = 0;
    @Override
    protected void loop()
    {

        tick();
        if(tickDuration <= 0) return;
        try {
            sleep(Math.max(tickDuration - System.currentTimeMillis() - lastTick,0));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        lastTick = System.currentTimeMillis();
    }
}
