package org.bitbucket.backspace119.generallib.io.network;

/**
 * Created by Bryan on 4/26/2017.
 */
public interface QueuedNetworkManager extends NetworkManager{
    NetworkQueue getQueue();
}
