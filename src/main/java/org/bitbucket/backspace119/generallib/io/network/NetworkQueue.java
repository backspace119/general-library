package org.bitbucket.backspace119.generallib.io.network;

import java.util.List;

/**
 * Created by Bryan on 4/26/2017.
 *
 * This class will most likely be subject to multi-thread handling, implementations should take care
 * to make themselves threadsafe
 *
 */
public interface NetworkQueue {

    Packet getNext();
    Packet getPrevious();
    List<Packet> getAll();
    void clear();
    void add(Packet p);



}
