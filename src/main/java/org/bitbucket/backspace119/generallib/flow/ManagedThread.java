package org.bitbucket.backspace119.generallib.flow;

/**
 * Created by Bryan on 3/18/2017.
 */
public abstract class ManagedThread extends Thread{

    protected ThreadManager threadManager;
    public ManagedThread(ThreadManager threadManager, String id)
    {
        this.threadManager = threadManager;
        threadManager.registerThread(this,id);
    }
    protected boolean run = true;
    public void quit()
    {
        run = false;
    }

    @Override
    public void run()
    {
        preLoop();
        while(true) {
            if (!run) break;
            loop();
        }
        postLoop();
    }
    public boolean isRunning()
    {
        return run;
    }

    protected abstract void loop();

    protected abstract void postLoop();

    protected abstract void preLoop();
}
