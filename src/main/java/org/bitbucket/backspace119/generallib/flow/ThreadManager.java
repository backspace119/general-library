package org.bitbucket.backspace119.generallib.flow;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Bryan on 3/18/2017.
 */
public class ThreadManager {

    private Map<String,ManagedThread> threadMap = new HashMap<>();
    public void registerThread(ManagedThread thread, String id)
    {
        threadMap.put(id,thread);
    }

    public ManagedThread getThread(String id)
    {
        return threadMap.get(id);
    }


    public void stopAllThreads()
    {
        for(String id:threadMap.keySet())
        {
            threadMap.get(id).quit();
        }
    }


}
