package org.bitbucket.backspace119.generallib.Logging;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;
import org.apache.logging.log4j.core.config.Configurator;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Bryan on 3/20/2017.
 */
public class LogMan {

    private Logger log;
    private LogFactory factory;
    private Map<String,Logger> logMap = new HashMap<>();
    public LogMan(LogFactory factory)
    {
        this.factory = factory;
    }


    public Logger createLogger(String ID,String output,Level level)
    {

        logMap.put(ID,factory.createLogger(ID,output,level));
        return logMap.get(ID);
    }
    public Logger getLogger(String ID)
    {
        return logMap.get(ID);
    }


}
