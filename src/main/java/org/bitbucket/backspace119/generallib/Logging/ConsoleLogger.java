package org.bitbucket.backspace119.generallib.Logging;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.appender.ConsoleAppender;
import org.apache.logging.log4j.core.config.Configurator;
import org.apache.logging.log4j.core.config.builder.api.*;
import org.apache.logging.log4j.core.config.builder.impl.BuiltConfiguration;

/**
 * Created by Bryan on 5/10/2017.
 */
public class ConsoleLogger implements LogFactory {
    @Override
    public Logger createLogger(String ID, String output, Level level) {

        ConfigurationBuilder<BuiltConfiguration> builder = ConfigurationBuilderFactory.newConfigurationBuilder();

        builder.setStatusLevel( Level.INFO);
        builder.setConfigurationName("RollingBuilder");
// create a console appender
        AppenderComponentBuilder appenderBuilder = builder.newAppender("Stdout", "CONSOLE").addAttribute("target",
                ConsoleAppender.Target.SYSTEM_OUT);
        appenderBuilder.add(builder.newLayout("PatternLayout")
                .addAttribute("pattern", "%d [%t] %-5level: %msg%n%throwable"));
        builder.add( appenderBuilder );
// create a rolling file appender


// create the new logger


        //builder.add(builder.newLogger(ID + "console",Level.INFO) . add(builder.newAppenderRef("Stdout")));
        builder.add( builder.newRootLogger( Level.DEBUG )
                .add( builder.newAppenderRef( "rolling" ) ).add(builder.newAppenderRef("Stdout")) );

        LoggerContext ctx = Configurator.initialize(builder.build());


        Logger logger = ctx.getLogger(ID);


        return logger;
    }
}
