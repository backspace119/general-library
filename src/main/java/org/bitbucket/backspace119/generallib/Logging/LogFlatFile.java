package org.bitbucket.backspace119.generallib.Logging;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.*;
import org.apache.logging.log4j.core.appender.ConsoleAppender;
import org.apache.logging.log4j.core.config.*;
import org.apache.logging.log4j.core.config.builder.api.*;
import org.apache.logging.log4j.core.config.builder.impl.BuiltConfiguration;

/**
 * Created by Bryan on 3/20/2017.
 */
public class LogFlatFile implements LogFactory{

    @Override
    public Logger createLogger(String ID,String targetLog, Level level)
    {



        ConfigurationBuilder<BuiltConfiguration> builder = ConfigurationBuilderFactory.newConfigurationBuilder();

        builder.setStatusLevel( Level.INFO);
        builder.setConfigurationName("RollingBuilder");
// create a console appender
        AppenderComponentBuilder appenderBuilder = builder.newAppender("Stdout", "CONSOLE").addAttribute("target",
                ConsoleAppender.Target.SYSTEM_OUT);
        appenderBuilder.add(builder.newLayout("PatternLayout")
                .addAttribute("pattern", "%d [%t] %-5level: %msg%n%throwable"));
        builder.add( appenderBuilder );
// create a rolling file appender
        LayoutComponentBuilder layoutBuilder = builder.newLayout("PatternLayout")
                .addAttribute("pattern", "%d [%t] %-5level: %msg%n");
        ComponentBuilder triggeringPolicy = builder.newComponent("Policies")
                .addComponent(builder.newComponent("CronTriggeringPolicy").addAttribute("schedule", "0 0 0 * * ?"))
                .addComponent(builder.newComponent("SizeBasedTriggeringPolicy").addAttribute("size", "100M"));
        AppenderComponentBuilder appenderBuilder2 = builder.newAppender("rolling", "RollingFile")
                .addAttribute("fileName", "log/rolling.log")
                .addAttribute("filePattern", "log/archive/rolling-%d{MM-dd-yy}.log.gz")
                .add(layoutBuilder)
                .addComponent(triggeringPolicy);
        builder.add(appenderBuilder2);

// create the new logger
        builder.add( builder.newLogger( ID, level )
                .add( builder.newAppenderRef( "rolling" ) )
                .addAttribute( "additivity", false ).add(builder.newAppenderRef("Stdout")) );

        //builder.add(builder.newLogger(ID + "console",Level.INFO) . add(builder.newAppenderRef("Stdout")));
        builder.add( builder.newRootLogger( Level.DEBUG )
                .add( builder.newAppenderRef( "rolling" ) ).add(builder.newAppenderRef("Stdout")) );

        LoggerContext ctx = Configurator.initialize(builder.build());


        Logger logger = ctx.getLogger(ID);


        return logger;
    }
}
