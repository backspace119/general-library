package org.bitbucket.backspace119.generallib.Logging;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.Logger;

/**
 * Created by Bryan on 3/20/2017.
 */
public interface LogFactory {

    Logger createLogger(String ID, String output, Level level);
}
